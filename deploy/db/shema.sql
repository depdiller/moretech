create type user_type as enum ('admin', 'developer', 'hr', 'employee', 'redactor');

CREATE TABLE IF NOT EXISTS transaction
(
    from_private_key varchar,
    to_public_key    varchar,
    hash varchar,
    primary key (hash)
);
create index public_key on transaction(to_public_key);
create index private_key on transaction(from_private_key);

CREATE TABLE IF NOT EXISTS wallet
(
    hash_transaction varchar,
    public_key     varchar,
    private_key    varchar unique,
    primary key (public_key),
    CONSTRAINT fk FOREIGN KEY (hash_transaction) REFERENCES "transaction" (hash)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);
create index wallet_private_key on wallet(private_key);
create index hash_transaction on wallet(hash_transaction);

CREATE TABLE IF NOT EXISTS nft
(
    token bigint primary key,
    uri varchar,
    public_key varchar,
    CONSTRAINT fk FOREIGN KEY (public_key) REFERENCES "wallet" (public_key)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);
create index nft_public_key on nft(public_key);
create index nft_uri on nft(uri);

CREATE TABLE IF NOT EXISTS item
(
    id    bigint generated always as identity,
    price money null,
    count bigint,
    primary key (id)
);

CREATE TABLE IF NOT EXISTS "user"
(
    id          bigint generated always as identity primary key,
    id_wallet   varchar,
    rating      bigint,
    name        varchar,
    second_name varchar,
    status      user_type not null,
    CONSTRAINT fk FOREIGN KEY (id_wallet) REFERENCES "wallet" (public_key)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);
create index user_wallet on "user" (id_wallet);
create index user_name on "user"(name);
create index user_second_name on "user"(second_name);

CREATE TABLE IF NOT EXISTS userItem
(
    id_item bigint references "item" (id),
    id_user bigint references "user" (id)
);
