import { TestBed } from '@angular/core/testing';

import { VtbAppService } from './vtb-app.service';

describe('VtbAppService', () => {
  let service: VtbAppService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VtbAppService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
