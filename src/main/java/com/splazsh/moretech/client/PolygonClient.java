package com.splazsh.moretech.client;

import com.splazsh.moretech.dto.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;

@FeignClient(value = "PolygonClient",
    url = "${variables.polygon.baseUrl}")
public interface PolygonClient {
    @RequestMapping(path = "${polygon.api.wallets.url}/new",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    CreateWalletResponse createWallet();

    @RequestMapping(path = "${polygon.api.wallets.url}/{publicKey}/balance",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    BalanceResponse checkBalance(@PathVariable String publicKey);

    @RequestMapping(path = "${polygon.api.wallets.url}/{publicKey}/nft/balance",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    BalanceNFTResponse checkNftBalance(@PathVariable String publicKey);

    @RequestMapping(path = "${polygon.api.transfers.url}/matic",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    TransferResponse transferMatic(@RequestBody TransferRequest transferRequest);

    @RequestMapping(path = "${polygon.api.transfers.url}/ruble",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    TransferResponse transferRuble(@RequestBody TransferRequest transferRequest);

    @RequestMapping(path = "${polygon.api.transfers.url}/nft",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    TransferResponse transferNft(@RequestBody TransferNftRequest transferNftRequest);

    @RequestMapping(path = "${polygon.api.transfers.url}/status/{transactionHash}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    TransferStatusResponse getTransactionStatus(@PathVariable String transactionHash);

    @RequestMapping(path = "${polygon.api.nft.url}/generate",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    GenerationNftResponse generateNft(@RequestBody GenerateNftRequest generateNftRequest);

    @RequestMapping(path = "${polygon.api.nft.url}/{tokenId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    NftUriResponse getNftInfo(@PathVariable BigInteger tokenId);
}