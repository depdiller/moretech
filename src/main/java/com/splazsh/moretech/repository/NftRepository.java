package com.splazsh.moretech.repository;

import com.splazsh.moretech.model.Nft;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

public interface NftRepository extends JpaRepository<Nft, BigInteger> {
}
