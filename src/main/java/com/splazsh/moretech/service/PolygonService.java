package com.splazsh.moretech.service;

import com.splazsh.moretech.client.PolygonClient;
import com.splazsh.moretech.dto.*;
import com.splazsh.moretech.model.Nft;
import com.splazsh.moretech.model.Transaction;
import com.splazsh.moretech.model.Wallet;
import com.splazsh.moretech.repository.NftRepository;
import com.splazsh.moretech.repository.TransactionRepository;
import com.splazsh.moretech.repository.WalletRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
@AllArgsConstructor
public class PolygonService {
    private final PolygonClient polygonClient;
    private final WalletRepository walletRepository;
    private final TransactionRepository transactionRepository;
    private final NftRepository nftRepository;

    public Wallet getWallet(String idWallet) {
        return walletRepository.getReferenceById(idWallet);
    }

    public void deleteWallet(String idWallet) {
        walletRepository.deleteById(idWallet);
    }

    public String getPrivateKey(String idWallet) {
        Wallet wallet = walletRepository.getReferenceById(idWallet);
        return wallet.getPrivateKey();
    }
    public Wallet createWallet() {
        CreateWalletResponse createWalletResponse = polygonClient.createWallet();
        Wallet wallet = Wallet.builder()
                .privateKey(createWalletResponse.getPrivateKey())
                .publicKey(createWalletResponse.getPublicKey())
                .build();
        return walletRepository.save(wallet);
    }

    public void transferMatic(TransferRequest transferRequest) {
        TransferResponse transferResponse = polygonClient.transferMatic(transferRequest);
        Transaction transaction = Transaction.builder()
                .fromPrivateKey(transferRequest.getFromPrivateKey())
                .toPublicKey(transferRequest.getToPublicKey())
                .hashTransaction(transferResponse.getTransactionHash())
                .build();
        transactionRepository.save(transaction);
    }

    public void transferRuble(TransferRequest transferRequest) {
        TransferResponse transferResponse = polygonClient.transferRuble(transferRequest);
        Transaction transaction = Transaction.builder()
                .fromPrivateKey(transferRequest.getFromPrivateKey())
                .toPublicKey(transferRequest.getToPublicKey())
                .hashTransaction(transferResponse.getTransactionHash())
                .build();
        transactionRepository.save(transaction);
    }

    public void transferNft(TransferNftRequest transferNftRequest) {
        TransferResponse transferResponse = polygonClient.transferNft(transferNftRequest);
        Transaction transaction = Transaction.builder()
                .fromPrivateKey(transferNftRequest.getFromPrivateKey())
                .toPublicKey(transferNftRequest.getToPublicKey())
                .hashTransaction(transferResponse.getTransactionHash())
                .build();
        transactionRepository.save(transaction);
    }

    public NftUriResponse getNftInfo(BigInteger tokenId) {
        return polygonClient.getNftInfo(tokenId);
    }

    public TransferStatusResponse getTransferStatus(String transactionHash) {
        return polygonClient.getTransactionStatus(transactionHash);
    }

    public BalanceNFTResponse getNftBalance(String publicKey) {
        return polygonClient.checkNftBalance(publicKey);
    }

    public BalanceResponse getBalance(String publicKey) {
        return polygonClient.checkBalance(publicKey);
    }

    public void generateNft(GenerateNftRequest generateNftRequest) {
        GenerationNftResponse generationNftResponse = polygonClient.generateNft(generateNftRequest);
        Wallet wallet = walletRepository.getReferenceById(generationNftResponse.getToPublicKey());
        for (BigInteger token: generationNftResponse.getTokens()) {
            Nft nft = Nft.builder()
                    .token(token)
                    .wallet(wallet)
                    .uri(generateNftRequest.getUri())
                    .build();
            nftRepository.save(nft);
        }
    }
}