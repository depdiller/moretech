package com.splazsh.moretech.service;

import com.splazsh.moretech.Types.UserType;
import com.splazsh.moretech.model.User;
import com.splazsh.moretech.model.Wallet;
import com.splazsh.moretech.repository.UserRepository;
import com.splazsh.moretech.repository.WalletRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    final private UserRepository userRepository;
    final private PolygonService polygonService;

    public void createUser(User userEntity) {
        Wallet wallet = polygonService.createWallet();
        User user = this.userRepository.save(userEntity);
        user.setIdWallet(wallet.getPublicKey());
    }

    public String getUserWalletPrivateKey(User user) {
        return polygonService.getPrivateKey(user.getIdWallet());
    }

    public void deleteUser(BigInteger userId) {
        User user = userRepository.getReferenceById(userId);
        polygonService.deleteWallet(user.getIdWallet());
    }

    public User getUserById(BigInteger userId) {
        return userRepository.getReferenceById(userId);
    }

    public List<User> getAllUser(){
        return this.userRepository.findAll();
    }


    public UserType getUserType(BigInteger id){
        User user = userRepository.getReferenceById(id);
        return user.getStatus();
    }

}