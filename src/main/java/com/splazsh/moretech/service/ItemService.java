package com.splazsh.moretech.service;

import com.splazsh.moretech.Types.UserType;
import com.splazsh.moretech.model.CreateItemRequest;
import com.splazsh.moretech.model.DeleteItemRequest;
import org.springframework.stereotype.Service;

@Service
public class ItemService {
    public long createItem(CreateItemRequest request){
        if (request.getUser().equals(UserType.ADMIN)){
            return 0L;
        }
        return 0L;
    }
    public void updateItem(CreateItemRequest request){
        if (request.getUser().equals(UserType.ADMIN)){

        }
    }

    public void deleteItem(DeleteItemRequest request){
        if (request.getUserType().equals(UserType.ADMIN)){

        }
    }
    public void getItemById(long id){

    }
    public void getAllItems(){}
}
