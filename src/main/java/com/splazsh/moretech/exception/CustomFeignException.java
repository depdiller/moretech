package com.splazsh.moretech.exception;

import lombok.Getter;

public class CustomFeignException extends RuntimeException {
//    @Getter
//    private GenericError error;
    public CustomFeignException(GenericError error) {
        super(error.getErrorMessage());
//        this.error = error;
    }
}
