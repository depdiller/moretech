package com.splazsh.moretech.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class GenericError {
    private String errorCode;
    private String errorMessage;
}
