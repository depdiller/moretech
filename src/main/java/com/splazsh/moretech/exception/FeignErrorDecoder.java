package com.splazsh.moretech.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
public class FeignErrorDecoder implements ErrorDecoder {
//    private final ErrorDecoder defaultErrorDecoder = new Default();

    @Override
    public Exception decode(String methodKey, Response response) {
        GenericError error;
        try (InputStream bodyIs = response.body().asInputStream()) {
            ObjectMapper mapper = new ObjectMapper();
            error = mapper.readValue(bodyIs, GenericError.class);
            return new CustomFeignException(error);
        } catch (IOException e) {
            return new Exception(e.getMessage());
        }
//        return defaultErrorDecoder.decode(methodKey, response);
    }
}