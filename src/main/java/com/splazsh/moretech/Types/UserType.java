package com.splazsh.moretech.Types;

import lombok.Getter;

@Getter
public enum UserType {
    ADMIN("ADMIN"),
    DEVELOPER("DEVELOPER"),
    HR("HR"),
    EMPLOYEE("EMPLOYEE"),
    REDACTOR("REDACTOR");
    private final String val;

    UserType(String val) {
        this.val = val;
    }

}
