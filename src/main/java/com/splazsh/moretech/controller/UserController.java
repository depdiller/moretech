//package com.splazsh.moretech.controller;
//
//import com.splazsh.moretech.model.CreateUserRequest;
//import com.splazsh.moretech.model.User;
//import com.splazsh.moretech.service.UserService;
//import io.swagger.v3.oas.annotations.Operation;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.*;
//
//import java.math.BigInteger;
//import java.util.List;
//
//
//@Controller
//public class UserController {
//    final private UserService userService;
//
//    public UserController(UserService userService) {
//        this.userService = userService;
//    }
//
//    @Operation(summary = "Create user")
//    @PostMapping("/createuser")
//    public void createUser(@Validated CreateUserRequest request) {
//         this.userService.createUser(User.builder()
//                .name(request.getName())
//                .secondName(request.getSecondNamed())
//                .status(request.getUserType())
//                .build());
//
//    }
//
//    @Operation(summary = "Delete user")
//    @DeleteMapping("/deleteuser/{id}")
//    public void deleteUserById(@PathVariable("id") BigInteger userId) {
//        this.userService.deleteUser(userId);
//    }
//
//    @Operation(summary = "Get all user")
//    @GetMapping("/getallusers")
//    public ResponseEntity<List<User>> getAllUsers() {
//        return ResponseEntity.created(null).body(this.userService.getAllUser());
//    }
//
//    @Operation(summary = "Get user by id")
//    @GetMapping("/getuser/{id}")
//    public ResponseEntity<User> getUserById(@PathVariable("id") long userId) {
//        return ResponseEntity.created(null).body(this.userService.getUserById(userId));
//    }
//
//}
