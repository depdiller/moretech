//package com.splazsh.moretech.controller;
//
//import com.splazsh.moretech.model.CreateItemRequest;
//import com.splazsh.moretech.model.DeleteItemRequest;
//import com.splazsh.moretech.service.ItemService;
//import lombok.Getter;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//
//@Controller
//public class ItemController {
//    final private ItemService itemService;
//
//    public ItemController(ItemService itemService) {
//        this.itemService = itemService;
//    }
//
//    @PostMapping("/item/create-item")
//    public long createItem(CreateItemRequest request) {
//        return this.itemService.createItem(request);
//    }
//
//    @PutMapping("/item/update")
//    public void updateItem(CreateItemRequest request) {
//        this.itemService.updateItem(request);
//    }
//
//    @DeleteMapping("/item/delete-item")
//    public void deleteItem(DeleteItemRequest request) {
//        this.itemService.deleteItem(request);
//    }
//
//    @GetMapping("/item/{id}")
//    public void getItemById(@PathVariable long id) {
//
//    }
//    @GetMapping("/item/")
//    public void getAllItems() {
//        this.itemService.getAllItems();
//    }
//}
