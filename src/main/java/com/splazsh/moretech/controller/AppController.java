package com.splazsh.moretech.controller;

import com.splazsh.moretech.dto.*;
import com.splazsh.moretech.model.*;
import com.splazsh.moretech.service.PolygonService;
import com.splazsh.moretech.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;

@RestController("app")
@AllArgsConstructor
public class AppController {
    private final PolygonService polygonService;
    private final UserService userService;

    @RequestMapping(path = "/nft/generate", method = RequestMethod.POST)
    public ResponseEntity<?> generateNft(GenerateNftRequest generateNftRequest) {
        polygonService.generateNft(generateNftRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/exchange/nft", method = RequestMethod.POST)
    public ResponseEntity<?> exchangeNft(NftExchangeRequest nftExchangeRequest) {
        User sender = userService.getUserById(nftExchangeRequest.getIdSender());
        User recipient = userService.getUserById(nftExchangeRequest.getIdRecipient());
        TransferNftRequest transferNftRequest = TransferNftRequest.builder()
                .fromPrivateKey(userService.getUserWalletPrivateKey(sender))
                .toPublicKey(recipient.getIdWallet())
                .tokenId(nftExchangeRequest.getToken())
                .build();
        polygonService.transferNft(transferNftRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/exchange/ruble", method = RequestMethod.POST)
    public ResponseEntity<?> exchangeRuble(RubleExchangeRequest rubleExchangeRequest) {
        User sender = userService.getUserById(rubleExchangeRequest.getIdSender());
        User recipient = userService.getUserById(rubleExchangeRequest.getIdRecipient());
        TransferRequest transferNftRequest = TransferRequest.builder()
                .fromPrivateKey(userService.getUserWalletPrivateKey(sender))
                .toPublicKey(recipient.getIdWallet())
                .amount(rubleExchangeRequest.getAmount())
                .build();
        polygonService.transferRuble(transferNftRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/balance/nft/{idUser}", method = RequestMethod.GET)
    public ResponseEntity<BalanceNFTResponse> checkNftBalance(@PathVariable BigInteger idUser) {
        User user = userService.getUserById(idUser);
        BalanceNFTResponse nftBalance = polygonService.getNftBalance(user.getIdWallet());
        return new ResponseEntity<>(nftBalance, HttpStatus.OK);
    }

    @RequestMapping(path = "/balance/rubles/{idUser}", method = RequestMethod.GET)
    public ResponseEntity<BalanceResponse> checkBalance(@PathVariable BigInteger idUser) {
        User user = userService.getUserById(idUser);
        BalanceResponse balance = polygonService.getBalance(user.getIdWallet());
        return new ResponseEntity<>(balance, HttpStatus.OK);
    }

    @RequestMapping(path = "/certificate/view/{token}", method = RequestMethod.GET)
    public ResponseEntity<String> viewCertificate(@PathVariable BigInteger token) {
        NftUriResponse nftUriResponse = polygonService.getNftInfo(token);
        String uri = nftUriResponse.getUri();
        return new ResponseEntity<>(uri, HttpStatus.OK);
    }
}
