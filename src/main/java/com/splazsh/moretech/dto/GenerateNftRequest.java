package com.splazsh.moretech.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GenerateNftRequest {
    private String toPublicKey;
    private String uri;
    private BigInteger nftCount;
}
