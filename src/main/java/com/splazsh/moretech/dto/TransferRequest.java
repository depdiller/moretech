package com.splazsh.moretech.dto;

import lombok.*;
import org.bouncycastle.pqc.math.linearalgebra.BigEndianConversions;

import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Builder
public class TransferRequest {
    private String fromPrivateKey;
    private String toPublicKey;
    private BigDecimal amount;
}
