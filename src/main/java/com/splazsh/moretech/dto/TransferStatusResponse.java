package com.splazsh.moretech.dto;

import com.splazsh.moretech.model.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TransferStatusResponse {
    private Status status;
}
