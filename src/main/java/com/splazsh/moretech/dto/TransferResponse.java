package com.splazsh.moretech.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
public class TransferResponse {
    private String transactionHash;
}
