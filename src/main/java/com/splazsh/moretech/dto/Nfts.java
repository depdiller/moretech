package com.splazsh.moretech.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class Nfts {
    private String uri;
    private List<BigInteger> tokens;
}
