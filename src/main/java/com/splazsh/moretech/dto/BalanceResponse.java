package com.splazsh.moretech.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Setter
public class BalanceResponse {
    private BigDecimal rublesAmount;
    private BigDecimal maticAmount;
}
