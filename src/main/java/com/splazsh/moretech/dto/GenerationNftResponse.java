package com.splazsh.moretech.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class GenerationNftResponse {
    private String toPublicKey;
    private List<BigInteger> tokens;
}
