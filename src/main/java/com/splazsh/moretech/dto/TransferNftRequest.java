package com.splazsh.moretech.dto;

import lombok.*;

import java.math.BigInteger;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@Setter
public class TransferNftRequest {
    private String fromPrivateKey;
    private String toPublicKey;
    private BigInteger tokenId;
}
