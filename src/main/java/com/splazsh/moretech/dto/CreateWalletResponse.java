package com.splazsh.moretech.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CreateWalletResponse {
    private String publicKey;
    private String privateKey;
}
