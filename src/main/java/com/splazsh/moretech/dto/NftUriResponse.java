package com.splazsh.moretech.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NftUriResponse {
    private BigInteger tokenId;
    private String uri;
    private String publicKey;
}
