package com.splazsh.moretech.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Wallet {
    @Id
    private String publicKey;

    private String privateKey;

    @OneToMany(mappedBy = "hashTransaction")
    @Cascade(CascadeType.ALL)
    private Set<Transaction> hashTransaction;

    @OneToMany(mappedBy = "token")
    private Set<Nft> nfts;
}
