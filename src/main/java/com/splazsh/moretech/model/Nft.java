package com.splazsh.moretech.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Nft {
    @Id
    private BigInteger token;
    private String uri;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "public_key")
    private Wallet wallet;
}
