package com.splazsh.moretech.model;

public enum Status {
    Success("Success"), Fail("Fail");
    private final String status;

    public String getStatus() {
        return status;
    }
    Status(String status) {
        this.status = status;
    }
}
