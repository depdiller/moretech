package com.splazsh.moretech.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {
    @Id
    private String hashTransaction;

    @Column(name = "from_private_key", insertable = false, updatable = false)
    private String fromPrivateKey;

    private String toPublicKey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "from_private_key")
    private Wallet wallet;
}
