package com.splazsh.moretech.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class WalletReplenishmentRequest {
    private String idSender;
    private String idRecipient;
    private BigDecimal amountOfMoney;
}
