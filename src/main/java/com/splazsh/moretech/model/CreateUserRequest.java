package com.splazsh.moretech.model;

import com.splazsh.moretech.Types.UserType;
import lombok.*;

import javax.validation.constraints.NotEmpty;

@Data
@Getter
@Setter
@Builder
public class CreateUserRequest {
    @NotEmpty
    String name;
    @NonNull
    String passwords;
    String secondNamed;
    @NotEmpty
    String email;
    UserType userType;
}
