package com.splazsh.moretech.model;

import com.splazsh.moretech.Types.UserType;
import lombok.Getter;

@Getter
public class DeleteItemRequest {
    long itemId;
    UserType userType;
}
