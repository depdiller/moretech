package com.splazsh.moretech.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class CertificateViewerRequest {
    private String idUser;
    private String idCertificate;
}
