package com.splazsh.moretech.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Setter
@Getter
public class CertificateExchangeProductsRequest {
    private String idUser;
    private String idCertificate;
    private List<Product> products;
}
