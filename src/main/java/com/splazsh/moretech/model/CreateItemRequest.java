package com.splazsh.moretech.model;

import com.splazsh.moretech.Types.UserType;
import lombok.*;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateItemRequest {
    @NotEmpty
    String name;
    long count;
    double price;
    @NotEmpty
    UserType user;
}
