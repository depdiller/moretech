package com.splazsh.moretech.model;

import com.splazsh.moretech.Types.UserType;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;
    String secondName;
    Integer rating;
    @Column(name = "id_wallet")
    String idWallet;

    @Enumerated(EnumType.ORDINAL)
    UserType status;
}
