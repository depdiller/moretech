package com.splazsh.moretech.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.BigInteger;

@Getter
@Setter
@NoArgsConstructor
public class RubleExchangeRequest {
    private BigInteger idSender;
    private BigInteger idRecipient;
    private BigDecimal amount;
}
