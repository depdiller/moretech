FROM maven:3.8.3-openjdk-17

WORKDIR /app

COPY . .
RUN mvn package
EXPOSE 8080

ENTRYPOINT [ "java", "-jar", "target/yandex-backend-task-1.0.0.jar" ]
